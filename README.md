StreamDevice

10/2022

IRFU/DIS/LDISC

# StreamDevice TP

Sur la machine virtuelle fournie\.

Télecharger le repo git : [https://gitlab\.com/formation\-epics/anf\-2022/2\_2\_Streamdevice](https://gitlab.com/formation-epics/anf-2022/2_2_Streamdevice)

# Kameleon rapide présentation

* Script python développé par l’équipe control system de l’accélérateur ESS\.  Il permet de simuler différent protocol TCP/IP\. \(Modbus\, RS485 \.\. Etc\)
  * \<Dépôt public introuvable\, se renseigner auprès d’ESS\, et voir si ils maintiennent l’outil\, sinon à voir comment le présenter aux stagiaires>
  * Prend en entrée un fichier .kam qui définit les commandes que l’on veut simuler
```Python
TERMINATOR = CR + LF
COMMANDS = [["Set Power",        "AC0",      1],
["Set Power",        "AC1",      1],
["Get Power Status", "AC?",      1],
["Get FIXED",        "FIXED?",   2],
["Get ENUM",         "ENUM?",    3],
["Get INCR",         "INCR?",    4],
["Get RANDOM",       "RANDOM?",  5],
["Get CUSTOM 1",     "CUSTOM1?", 6],
["Get CUSTOM 2",     "CUSTOM2?", 7]]
STATUSES = [["Get Power Status", ENUM,   [1, 0], "AC?"],
["Get FIXED",        FIXED,  18.3],
["Get ENUM",         ENUM,   [10, 20, 30, 40, 50]],
["Get INCR",         INCR,   2],
["Get RANDOM",       RANDOM, 50],
["Get CUSTOM 1",     CUSTOM, "my_function_1()"],
["Get CUSTOM 2",     CUSTOM, "my_function_2()"]]

# User-custom Python code
my_value = 0

def my_function_1():
  global my_value
  my_value = my_value + 1
  return math.sin(0.25 * my_value) * 200 + random.randrange(1, 40)  # calculate a sinusoid + some noise and return the result to the client

def my_function_2():

  data = COMMAND_RECEIVED  # variable COMMAND_RECEIVED contains the data received from the client (this variable is managed by Kameleon)
  return data.lower()  # transform the data received in lower case and return the result to the client
```
# StreamDevice TP

####Préparation au TP

* Simulation de l’appareil à l’aide du script python d’ESS « Kameleon »
  * Dans « simulationTdkLambda »
  * python2\.7 kameleon\.py \-\-file=tdk\_lambda\_genesys\.kam \-\-host=127\.0\.0\.1 \-\-port=8003
  * la commande est dans le readme pour copier coller
* Maintenant vous avez une alimentation virtuelle \(non exhaustive\) qui répond sur votre machine virtuelle au port 8003
* Créer un dossier pour votre top
* Dans ce dossier créer un App et un IOC
* makeBaseApp\.pl \-a linux\-x86\_64 \-t ioc tdkLambdaGen && makeBaseApp\.pl \-a linux\-x86\_64 \-i \-t ioc \-p tdkLambdaGen tdkLambdaGen
* Dans ce dossier créer un App et un IOC
* Ajouter les modules CALC\, ASYN\, STREAM à la RELEASE de votre TOP

####Exercice 1 : Le contrôle d’une alimentation TDK Lambda Genesys

* Astuce pour tester les commandes aussi vrai pour cet exercice qu’en cas pratique\, utiliser telnet:
  * telnet 127\.0\.0\.1 8003

<span style="color:#FF0000">ATTENTION : notre simulateur est moins permissif que le vrai appareil les commandes utilisées auront la syntaxe suivante :</span>

<span style="color:#FF0000">MEAS:VOLT?</span>

<span style="color:#FF0000">Ne sera pas accepté les formes suivantes malgré ce que dit la doc de l’appareil:</span>  <span style="color:#FF0000">MEASure:VOLTage</span>  <span style="color:#FF0000">?</span>  <span style="color:#FF0000">meas:volt</span>  <span style="color:#FF0000">?</span>  <span style="color:#FF0000">Etc</span>  <span style="color:#FF0000"> …</span>


* A l’aide du user manual dans « doc/manualblabla\.pdf » p
  * Ajouter des PVs pour allumer l’alimentation et voir son état \(bi/bo\)
    * Plus: Au caget afficher OFF et ON
  * Ajouter des PVs pour configurer et lire le voltage\. \(ai/ao\)
    * Plus : Au redémarrage de l’IOC le setter doit avoir la valeur déjà lu par l’appareil
  * Ajouter des PVs pour configurer et lire le courant\. \(ai/ao\)
    * Plus : Au redémarrage de l’IOC le setter doit avoir la valeur déjà lu par l’appareil
  * Ajouter des PVs pour changer le mode de prise en main de l’appareil \(Local\, Remote ou lock\) \(mbbi/mbbo\)
    * Plus: Au caget afficher CC\, CV ou OFF
  * Ajouter une PV pour voir la version de l’appareil \(waveform\)
    * Caget –S pour afficher un string
* Avancée
  * Ajouter 3 PVs pour lire la commande \*PIEGE? \(qui n’est pas dans la doc\)
    * Plus : Une seul PV envoie la commande à l’appareil\, les 2 autres écoutent\.
  * Lire courant de la command :PIEGE:CURR? \, et éviter d’avoir un message d’erreur dans la console de l’IOC

# 

