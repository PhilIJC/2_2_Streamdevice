#!../../bin/linux-x86_64/tdkLambdaGen

#- You may have to change tdkLambdaGen to something else
#- everywhere it appears in this file

< envPaths
epicsEnvSet(asynPort, "MYPORT")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(TOP)/db")
cd "${TOP}"
## Register all support components
dbLoadDatabase "dbd/tdkLambdaGen.dbd"
tdkLambdaGen_registerRecordDeviceDriver pdbbase


drvAsynIPPortConfigure("${asynPort}", "127.0.0.1:8003")

## Load record instances
dbLoadRecords("db/iocTDK_20_20.db","user=epicslearner, asynPort=$(asynPort)")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=epicslearner"
